package frenchdonuts.tasty.gankoptimizr.util

import frenchdonuts.tasty.gankoptimizr.views.base.BaseActivity
import frenchdonuts.tasty.gankoptimizr.views.base.BaseFragment
import org.funktionale.either.Either
import rx.Observable
import rx.Subscription

/**
 * Created by frenchdonuts on 3/11/16.
 */

fun <FTag> Subscription.managedBy(baseActivity: BaseActivity<FTag>) = baseActivity.compositeSubscription.add(this)
fun Subscription.managedBy(baseFragment: BaseFragment) = baseFragment.compositeSubscription.add(this)

infix fun <T, R> Observable<T>.then(next: (T) -> Observable<R>) = this.flatMap { next(it) }

// flatBiMap
fun <TEL, TER, REL, RER> Observable<Either<TEL, TER>>.then(unit: (TEL) -> REL, next: (TER) -> Observable<Either<REL, RER>>) =
        this.flatMap {
            when (it) {
                is Either.Right -> next(it.r)
                is Either.Left -> Observable.just(Either.Left<REL, RER>(unit(it.l)))
            }
        }

fun <L, R1, R2> Observable<Either<L, R1>>.eitherThen(next: (R1) -> Observable<Either<L, R2>>) =
    this.flatMap {
        when (it) {
            is Either.Right -> next(it.r)
            is Either.Left -> Observable.just(Either.Left<L, R2>(it.l))
        }
    }

fun <TEL, REL, RER> emitError(left: TEL) = Observable.just(left)

fun <L, R, C> Either<L, R>.either(f1: (L) -> C, f2: (R) -> C) =
        when (this) {
            is Either.Left -> f1(this.left().get())
            is Either.Right -> f2(this.right().get())
        }

