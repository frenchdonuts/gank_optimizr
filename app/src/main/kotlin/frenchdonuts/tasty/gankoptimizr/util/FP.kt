package frenchdonuts.tasty.gankoptimizr.util


/**
 * Created by frenchdonuts on 2/25/16.
 */
fun <A> id(a: A): A = a

fun <A> e(a: A): () -> A = { a }

fun <A> ((Unit) -> A).r() {
    this.invoke(Unit)
}

infix fun <A, B, C> ((A) -> B).then(g: (B) -> C): (A) -> C = {
    a ->
    g.invoke(this.invoke(a))
}

infix fun <B, C> (() -> B).then(g: (B) -> C): () -> C = {
    g.invoke(this.invoke())
}

infix fun <A, B, C> ((A) -> B).i(g: (B) -> C): (A) -> C = {
    a ->
    g.invoke(this.invoke(a))
}

fun <A> filter(xs: List<A>, pred: (A) -> Boolean) = xs.filter { pred(it) }
