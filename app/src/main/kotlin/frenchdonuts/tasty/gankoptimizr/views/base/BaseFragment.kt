package frenchdonuts.tasty.gankoptimizr.views.base

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import rx.subscriptions.CompositeSubscription

/**
 * Created by frenchdonuts on 4/29/16.
 *
 * Things my Fragments do:
 *  Inject RDB
 *  Inject any Repositories they need access to - App specific
 *  Bind the associated child Views - App specific
 *  Setup and execute queries to fetch data to display - App specific
 *  Dispatch Actions in response to View events - App specific
 *      aka Interpreting View events
 *  Hmmm...I don't see too much room for abstraction here.
 *    Except injecting RDB. But how do we abstract that away?
 */
open class BaseFragment : Fragment() {
    //
    val compositeSubscription: CompositeSubscription = CompositeSubscription()
    lateinit var navigator: Navigator

    // DI: App.graph.inject(this)
    override fun onAttach(context: Context?) {
        Log.d(this.javaClass.simpleName, "onAttach")
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        Log.d(this.javaClass.simpleName, "onActivityCreated")
        super.onActivityCreated(savedInstanceState)
        navigator = activity as Navigator
    }

    // return inflater?.inflate(R.layout.fragment_layout, container, false)
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(this.javaClass.simpleName, "onCreateView")
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    // Run Butterknife.bind(this, view), setup RecyclerView, etc.
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        Log.d(this.javaClass.simpleName, "onViewCreated")
        super.onViewCreated(view, savedInstanceState)
    }

    // Start data-fetching here
    override fun onStart() {
        Log.d(this.javaClass.simpleName, "onStart")
        super.onStart()
    }

    override fun onStop() {
        Log.d(this.javaClass.simpleName, "onStop")
        compositeSubscription.clear()
        super.onStop()
    }
}