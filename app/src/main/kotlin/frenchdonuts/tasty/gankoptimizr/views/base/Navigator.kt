package frenchdonuts.tasty.gankoptimizr.views.base

import io.onedonut.re_droid.Action

/**
 * Created by frenchdonuts on 5/9/2016.
 */
interface Navigator {
    fun handleAction(action: Action)
}