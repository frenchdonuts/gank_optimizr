package frenchdonuts.tasty.gankoptimizr.views

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import butterknife.Bind
import butterknife.ButterKnife
import com.jakewharton.rxbinding.view.clicks
import frenchdonuts.tasty.gankoptimizr.App
import frenchdonuts.tasty.gankoptimizr.R
import frenchdonuts.tasty.gankoptimizr.core.AppState
import frenchdonuts.tasty.gankoptimizr.core.FetchOpponentsRequest
import frenchdonuts.tasty.gankoptimizr.repositories.RiotRepo
import frenchdonuts.tasty.gankoptimizr.util.managedBy
import frenchdonuts.tasty.gankoptimizr.views.base.BaseFragment
import io.onedonut.re_droid.Action
import io.onedonut.re_droid.RDB
import io.onedonut.re_droid.middleware.RxEffect
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by frenchdonuts on 4/25/16.
 *
 */
class SummonerNameFragment : BaseFragment() {
    //
    @Inject
    lateinit var rdb: RDB<AppState>

    @Inject
    lateinit var riotRepo: RiotRepo


    @Bind(R.id.etSummonerName)
    lateinit var etSummonerName: EditText
    @Bind(R.id.btGetOpponents)
    lateinit var btGetOpponents: Button

    companion object {
        //
        val TAG = SummonerNameFragment::class.java.simpleName

        fun newInstance(): Fragment {
            return SummonerNameFragment();
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        App.graph.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //
        return inflater?.inflate(R.layout.fragment_summoner_name, container, false)
    }

    fun FetchOpponentsEffect(summonerName: String): Observable<Action> =
            riotRepo.resolveGetOpponentDossiersToAction(summonerName)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext { navigator.handleAction(it) }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        //
        super.onViewCreated(view, savedInstanceState)
        ButterKnife.bind(this, view)

        // Get Opponents when User presses button
        btGetOpponents.clicks()
                .throttleLast(100, TimeUnit.MILLISECONDS)
                .debounce(200, TimeUnit.MILLISECONDS)
                .filter { etSummonerName.text.length > 0 }
                .filter { !rdb.curAppState.isFetchingOpponents }
                .subscribe {
                    //
                    rdb.dispatch(FetchOpponentsRequest(TAG))
                    rdb.dispatch(RxEffect(FetchOpponentsEffect(etSummonerName.text.toString()), origin = TAG))
                }.managedBy(this)
    }
}