package frenchdonuts.tasty.gankoptimizr.views

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.Fragment
import android.widget.Toast
import frenchdonuts.tasty.gankoptimizr.App
import frenchdonuts.tasty.gankoptimizr.R
import frenchdonuts.tasty.gankoptimizr.core.*
import frenchdonuts.tasty.gankoptimizr.util.managedBy
import frenchdonuts.tasty.gankoptimizr.views.base.BaseActivity
import io.onedonut.re_droid.Action
import io.onedonut.re_droid.RDB
import io.onedonut.re_droid.utils.Two
import rx.android.schedulers.AndroidSchedulers
import javax.inject.Inject

/**
 * Created by frenchdonuts on 4/24/16.
 *
 */
class MainActivity : BaseActivity<MainActivity.FTag>() {
    //
    val TAG = MainActivity::class.java.simpleName

    val APPSTATE_KEY = "app_state_parcelable"

    @Inject
    lateinit var rdb: RDB<AppState>

    // Meant to document which Fragments this Activity manages
    sealed class FTag(val tag: String) {
        object SummonerName : FTag(SummonerNameFragment.TAG)

        object Opponents : FTag(OpponentsFragment.TAG)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        App.graph.inject(this);

        if (savedInstanceState != null) {

            // Restore AppState
            rdb.dispatch(RestoreState(savedInstanceState.getParcelable<AppStateParcel>(APPSTATE_KEY).data, TAG))
        } else {

            // Fragment to show on launch
            transactToFragment(FTag.SummonerName, R.id.mainContent)
        }
    }

    override fun handleAction(action: Action) {
        // This is where we will route Actions to appropriate Fragment transactions
        when (action) {
            is FetchOpponentsSuccess -> transactToFragment(FTag.Opponents, R.id.mainContent)
            else -> {
            }
        }
    }

    override fun makeFragment(ftag: FTag): Fragment {
        when (ftag) {
            is FTag.SummonerName -> return SummonerNameFragment.newInstance()
            is FTag.Opponents -> return OpponentsFragment.newInstance()
        }
    }

    val fetchOpponentErrorQuery =
            { s: AppState ->
                Two(
                        s.fetchOpponentError,
                        s.mainActivityShouldNotifyFetchOpponentError)
            }

    override fun onStart() {
        super.onStart()

        rdb.execute(fetchOpponentErrorQuery)
                .filter { it._1.length > 0 && it._2 }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    //
                    Toast.makeText(this, it._1, Toast.LENGTH_SHORT).show()
                    rdb.dispatch(UserNotifiedOfFetchOpponentsFailure(TAG))
                }.managedBy(this)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        // Save AppState
        outState?.putParcelable(APPSTATE_KEY, AppStateParcel(rdb.curAppState))

        // Call superclass so it can save the view hierarchy state
        super.onSaveInstanceState(outState)
    }
}