package frenchdonuts.tasty.gankoptimizr.views.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import io.onedonut.re_droid.Action
import rx.subscriptions.CompositeSubscription

/**
 * Created by frenchdonuts on 4/24/16.
 *
 * Activities will manage:
 *  Fragment navigation
 *  Toasts
 */
abstract open class BaseActivity<FTag> : AppCompatActivity(), Navigator {
    //
    val compositeSubscription : CompositeSubscription = CompositeSubscription()

    // setContentView(R.layout.activity_layout, App.graph.inject(this), etc.
    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(this.javaClass.simpleName, "onCreate")
        super.onCreate(savedInstanceState)
    }

    // Route Actions to appropriate FragmentTransactions
    abstract open fun makeFragment(fTag: FTag): Fragment
    // TODO: Parameterize by transition animation?
    fun transactToFragment(ftag: FTag, layoutId: Int) {
        //
        val fm: FragmentManager = supportFragmentManager
        var f: Fragment?

        f = fm.findFragmentByTag(ftag?.hashCode().toString());
        if (f == null)
            f = makeFragment(ftag);

        fm.beginTransaction()
                .replace(layoutId, f, ftag?.hashCode().toString())
                .addToBackStack(null)
                .commit();
    }

    // Subscribe to AppState here.
    override fun onStart() {
        Log.d(this.javaClass.simpleName, "onStart")
        super.onStart()
    }

    override fun onResume() {
        Log.d(this.javaClass.simpleName, "onResume")
        super.onResume()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        Log.d(this.javaClass.simpleName, "onSaveInstanceState")
        super.onSaveInstanceState(outState)
    }

    override fun onPause() {
        Log.d(this.javaClass.simpleName, "onPause")
        super.onPause()
    }

    override fun onStop() {
        Log.d(this.javaClass.simpleName, "onStop")
        compositeSubscription.clear();
        super.onStop()
    }

    override fun onDestroy() {
        Log.d(this.javaClass.simpleName, "onDestroy")
        super.onDestroy()
    }
}