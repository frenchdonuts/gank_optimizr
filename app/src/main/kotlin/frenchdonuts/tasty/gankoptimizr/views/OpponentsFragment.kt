package frenchdonuts.tasty.gankoptimizr.views

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.Bind
import butterknife.ButterKnife
import frenchdonuts.tasty.gankoptimizr.App
import frenchdonuts.tasty.gankoptimizr.R
import frenchdonuts.tasty.gankoptimizr.core.AppState
import frenchdonuts.tasty.gankoptimizr.core.models.Loler
import io.onedonut.re_droid.RDB
import io.onedonut.re_droid.utils.One
import javax.inject.Inject

/**
 * Created by frenchdonuts on 4/24/16.
 */
class OpponentsFragment : Fragment() {
    //
    @Inject
    lateinit var rdb: RDB<AppState>

    @Bind(R.id.rvOpponents)
    lateinit var rvOpponents: RecyclerView

    companion object {
        //
        val TAG = OpponentsFragment::class.java.simpleName

        fun newInstance(): Fragment {
            //
            return OpponentsFragment()
        }
    }

    override fun onAttach(context: Context?) {
        //
        super.onAttach(context)
        App.graph.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //
        return inflater?.inflate(R.layout.fragment_opponents, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        //
        ButterKnife.bind(this, view)

        // Attach Adapter to rvOpponents
        rvOpponents.adapter = Adapter(rdb)
        rvOpponents.layoutManager = LinearLayoutManager(context)
    }

    class Adapter(rdb: RDB<AppState>) : RecyclerView.Adapter<Adapter.OpponentViewHolder>() {
        //
        var opponents: List<Loler>

        init {
            //
            opponents = listOf()
            rdb.execute { One(it.opponents) }
                    .subscribe {
                        //
                        opponents = it._1
                        notifyDataSetChanged()
                    }
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): OpponentViewHolder? {
            //
            val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_opponent, parent, false)

            return OpponentViewHolder(view)
        }

        override fun onBindViewHolder(holder: OpponentViewHolder?, position: Int) {
            //
            holder?.bind(opponents[position])
        }

        override fun getItemCount(): Int {
            //
            return opponents.size
        }

        class OpponentViewHolder(val rootItemView: View) : RecyclerView.ViewHolder(rootItemView) {
            //
            @Bind(R.id.tvOpponentName)
            lateinit var tvOpponentName: TextView

            @Bind(R.id.tvOpponentTier)
            lateinit var tvOpponentTier: TextView


            init {
                ButterKnife.bind(this, rootItemView)
            }

            fun bind(loler: Loler) {
                //
                tvOpponentName.text = loler.summonerName

                if (loler.leagues.isNotEmpty())
                    tvOpponentTier.text = loler.leagues[0].tier
                else
                    tvOpponentTier.text = "Rank Unavailable"
            }
        }
    }
}