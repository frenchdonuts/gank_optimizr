package frenchdonuts.tasty.gankoptimizr

import dagger.Module
import dagger.Provides
import frenchdonuts.tasty.gankoptimizr.core.AppState
import frenchdonuts.tasty.gankoptimizr.core.rootReducer
import io.onedonut.re_droid.RDB
import io.onedonut.re_droid.middleware.logActions
import io.onedonut.re_droid.middleware.rx
import rx.android.schedulers.AndroidSchedulers
import javax.inject.Singleton

/**
 * Created by frenchdonuts on 4/25/16.
 */
@Module
class RDBModule {

    @Provides
    @Singleton
    fun provideRDB(): RDB<AppState> =
            RDB(
                    AppState(),
                    rootReducer,
                    // Run our rootReducer on the main thread
                    AndroidSchedulers.mainThread(),
                    // Middleware - gets evaluated from right to left
                    { rx<AppState>(mutableMapOf())(it) }, { logActions(it) }
            )

}