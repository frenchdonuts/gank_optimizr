package frenchdonuts.tasty.gankoptimizr.core

import frenchdonuts.tasty.gankoptimizr.core.models.Loler
import nz.bradcampbell.paperparcel.PaperParcel

/**
 * Created by frenchdonuts on 3/2/16.
 *
 */
@PaperParcel
data class AppState(val opponents: List<Loler> = listOf(), //TODO: Make a re-usable async-fetch component: T, isFetching, fetchError
                    val isFetchingOpponents: Boolean = false,
                    val fetchOpponentError: String = "",
                    val mainActivityShouldNotifyFetchOpponentError: Boolean = false)
