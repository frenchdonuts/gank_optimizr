package frenchdonuts.tasty.gankoptimizr.core.models

import frenchdonuts.tasty.gankoptimizr.services.riot_service.models.LeagueDto
import frenchdonuts.tasty.gankoptimizr.services.riot_service.models.Mastery
import frenchdonuts.tasty.gankoptimizr.services.riot_service.models.Rune
import nz.bradcampbell.paperparcel.PaperParcel

/**
 * Created by frenchdonuts on 3/4/16.
 *
 */
@PaperParcel
data class Loler(val masteries: List<Mastery> = listOf(),
                 val runes: List<Rune> = listOf(),
                 val summonerId: Long = Long.MIN_VALUE,
                 val summonerName: String = "",
                 val teamId: Long = Long.MIN_VALUE,
                 val leagues: List<LeagueDto> = listOf())
