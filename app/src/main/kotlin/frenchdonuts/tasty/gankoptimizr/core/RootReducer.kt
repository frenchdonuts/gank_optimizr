package frenchdonuts.tasty.gankoptimizr.core

import io.onedonut.re_droid.Action

/**
 * Created by frenchdonuts on 4/25/16.
 */

val rootReducer: (Action, AppState) -> AppState = {
    action, appState ->
        when (action) {
            is FetchOpponentsRequest -> appState.copy(isFetchingOpponents = true)
            is FetchOpponentsSuccess ->
                appState.copy(
                        opponents = action.opponents,
                        isFetchingOpponents = false,
                        fetchOpponentError = "",
                        mainActivityShouldNotifyFetchOpponentError = false
                )
            // TODO
            is FetchOpponentsFailure -> {
                appState.copy(
                        isFetchingOpponents = false,
                        fetchOpponentError = action.errMsg,
                        mainActivityShouldNotifyFetchOpponentError = true
                )
            }
            is UserNotifiedOfFetchOpponentsFailure -> appState.copy(
                    mainActivityShouldNotifyFetchOpponentError = false
            )

            is RestoreState -> action.state
            else -> appState
        }
}

