package frenchdonuts.tasty.gankoptimizr.core

import frenchdonuts.tasty.gankoptimizr.core.models.Loler
import io.onedonut.re_droid.Action

/**
 * Created by frenchdonuts on 4/29/16.
 *
 */
class RestoreState(val state: AppState, origin: String) : Action(origin) {
    override fun toString(): String {
        return "RestoreState: (state, $state), (origin, $origin)"
    }
}

class FetchOpponentsRequest(origin: String) : Action(origin)

class FetchOpponentsSuccess(val opponents: List<Loler>, origin: String) : Action(origin) {
    override fun toString(): String {
        return "SetOpponents: { (opponents, $opponents), (origin, $origin) }"
    }
}

class FetchOpponentsFailure(val errMsg: String, origin: String) : Action(origin) {
    override fun toString(): String {
        return "NotifyFetchOpponentError: { (errMsg, $errMsg), (origin, $origin) }"
    }
}

class UserNotifiedOfFetchOpponentsFailure(origin: String) : Action(origin)
