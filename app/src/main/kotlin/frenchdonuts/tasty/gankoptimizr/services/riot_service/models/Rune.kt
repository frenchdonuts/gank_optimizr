package frenchdonuts.tasty.gankoptimizr.services.riot_service.models

import nz.bradcampbell.paperparcel.PaperParcel

/**
 * Created by frenchdonuts on 3/4/16.
 *
 */
@PaperParcel
data class Rune(val count: Int, val runeId: Long)
