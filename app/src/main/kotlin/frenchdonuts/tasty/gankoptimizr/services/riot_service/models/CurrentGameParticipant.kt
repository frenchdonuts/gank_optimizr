package frenchdonuts.tasty.gankoptimizr.services.riot_service.models

/**
 * Created by frenchdonuts on 3/4/16.
 *
 */
data class CurrentGameParticipant(val masteries: List<Mastery>,
                                  val runes: List<Rune>,
                                  val summonerId: Long,
                                  val summonerName: String,
                                  val teamId: Long)
