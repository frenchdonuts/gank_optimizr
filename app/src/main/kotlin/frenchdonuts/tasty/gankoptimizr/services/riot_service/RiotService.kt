package frenchdonuts.tasty.gankoptimizr.services.riot_service

import frenchdonuts.tasty.gankoptimizr.services.riot_service.models.CurrentGameInfo
import frenchdonuts.tasty.gankoptimizr.services.riot_service.models.LeagueDto
import frenchdonuts.tasty.gankoptimizr.services.riot_service.models.SummonerDto
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Observable

/**
 * Created by frenchdonuts on 3/4/16.
 *
 */
interface RiotService {

    @GET("/api/lol/na/v1.4/summoner/by-name/{summonerName}")
    fun fetchSummonerId(@Path("summonerName") summonerName: String,
                        @Query("api_key") api_key: String): Observable<Map<String, SummonerDto>>

    // TODO: Make it so we can change the platformId (NA1) as well
    @GET("/observer-mode/rest/consumer/getSpectatorGameInfo/NA1/{summonerId}")
    fun fetchCurrentGame(@Path("summonerId") summonerId: String,
                         @Query("api_key") api_key: String): Observable<CurrentGameInfo>

    @GET("/api/lol/na/v2.5/league/by-summoner/{summonerIds}/entry")
    fun fetchRanks(@Path("summonerIds") summonerIds: String,
                   @Query("api_key") api_key: String): Observable<Map<String, List<LeagueDto>>>

    companion object {
        val TAG = RiotService::class.java.simpleName

        val API_URL = "https://na.api.pvp.net"
        val API_KEY = "8d94bfd2-5229-4735-a33b-b0e718554aec"

        //
        fun create(client: OkHttpClient): RiotService {

            val retrofit = Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(client)
                    .build();

            return retrofit.create(RiotService::class.java)
        }
    }
}