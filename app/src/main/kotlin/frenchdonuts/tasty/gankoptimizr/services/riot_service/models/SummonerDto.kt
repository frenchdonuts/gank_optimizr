package frenchdonuts.tasty.gankoptimizr.services.riot_service.models

/**
 * Created by frenchdonuts on 3/4/16.
 *
 */
data class SummonerDto(val id: Long)