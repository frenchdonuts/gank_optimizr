package frenchdonuts.tasty.gankoptimizr.services

import com.facebook.stetho.okhttp3.StethoInterceptor
import dagger.Module
import dagger.Provides
import frenchdonuts.tasty.gankoptimizr.services.riot_service.RiotService
import okhttp3.OkHttpClient
import javax.inject.Singleton

/**
 * Created by frenchdonuts on 4/25/16.
 */
@Module
class ServicesModule {

    @Provides
    @Singleton
    fun provideHttpClient(): OkHttpClient {
        //
        val client = OkHttpClient().newBuilder()
            .addNetworkInterceptor(StethoInterceptor())
            .build()

        return client
    }

    @Provides
    @Singleton
    fun provideRiotService(httpClient: OkHttpClient): RiotService {
        return RiotService.create(httpClient)
    }

}