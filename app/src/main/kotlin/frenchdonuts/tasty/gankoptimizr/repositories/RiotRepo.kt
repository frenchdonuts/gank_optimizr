package frenchdonuts.tasty.gankoptimizr.repositories

import android.util.Log
import frenchdonuts.tasty.gankoptimizr.core.FetchOpponentsFailure
import frenchdonuts.tasty.gankoptimizr.core.FetchOpponentsSuccess
import frenchdonuts.tasty.gankoptimizr.core.models.Loler
import frenchdonuts.tasty.gankoptimizr.services.riot_service.RiotService
import frenchdonuts.tasty.gankoptimizr.util.eitherThen
import io.onedonut.re_droid.Action
import org.funktionale.either.Either
import org.funktionale.either.Either.Right
import org.funktionale.either.Either.Left
import rx.Observable
import rx.schedulers.Schedulers

/**
 * Created by frenchdonuts on 5/9/2016.
 *
 * Repositories: The bridge between Services (data-sources) and our state transitions (Actions)
 */
class RiotRepo(val riotService: RiotService) {
    //
    val TAG: String = "RiotRepo"

    /**
     * Resolve to the proper state transition depending on the result of fetching Opponent Dossiers
     *
     * @param summonerName Name of the Summoner whose opponents to fetch
     */
    fun resolveGetOpponentDossiersToAction(summonerName: String): Observable<Action> =
            getOpponentDossiers(summonerName)
                    // Make sure we don't do these network calls on UI thread
                    .subscribeOn(Schedulers.io())
                    .map {
                        when (it) {
                            is Right -> FetchOpponentsSuccess(it.r, TAG)
                            is Left -> FetchOpponentsFailure(it.l, TAG)
                        }
                    }

    /**
     * Get a list of opponents : List<Loler> of the Summoner with summonerName
     *
     * @param summonerName Name of Summoner
     * @return Either an error msg or a list of Opponents
     */
    private fun getOpponentDossiers(summonerName: String): Observable<Either<String, List<Loler>>> =
            getSummonerIdOf(summonerName)
                    .eitherThen { getOpponentsOf(it) }
                    .eitherThen { getRanksOf(it) }

    /**
     * Get the summonerId
     *
     * @param summonerName The name of the Summoner
     */
    private fun getSummonerIdOf(summonerName: String): Observable<Either<String, Long>> =
            riotService
                    .fetchSummonerId(summonerName, RiotService.API_KEY)
                    .filter { !it.isEmpty() }
                    .map { Right<String, Long>(it.values.elementAt(0).id) as Either<String, Long> }
                    .onErrorReturn { Left("Summoner does not exist!") }

    /**
     * Get the current opponents of the Summoner given the summonerId
     *
     * @param summonerId The summonerId of the Summoner
     * @return A List<Loler> without tier information.
     */
    private fun getOpponentsOf(summonerId: Long): Observable<Either<String, List<Loler>>> =
            riotService
                    .fetchCurrentGame(summonerId.toString(), RiotService.API_KEY)
                    .map {
                        //
                        val teamId = it.participants.find { it.summonerId == summonerId }?.teamId as Long
                        it.participants.filter { it.teamId != teamId }.map {
                            Loler(it.masteries, it.runes, it.summonerId, it.summonerName, teamId)
                        }
                    }.map { Right<String, List<Loler>>(it) as Either<String, List<Loler>> }
                    .onErrorReturn { Left("Summoner not in game.") }


    /**
     * Get the opponents' info(materies, runes, rank, etc.)
     *
     * @param opponents The list of opponents
     * @return If the HTTP response is valid, a List<Loler> with tier information. Otherwise,
     *         the List<Loler> that was given
     */
    private fun getRanksOf(opponents: List<Loler>): Observable<Either<String, List<Loler>>> =
            riotService
                    .fetchRanks(opponents.map { it.summonerId }.joinToString(","), RiotService.API_KEY)
                    .map { summonerId_leagueDtoMap ->
                        Right<String, List<Loler>>(
                                opponents.map {
                                    // Fill in rank info
                                    if (summonerId_leagueDtoMap.contains(it.summonerId.toString()))
                                        it.copy(leagues = summonerId_leagueDtoMap[it.summonerId.toString()]!!)
                                    else it
                                }
                        ) as Either<String, List<Loler>>
                    }
                    // If this network request fails, we just emit what we have so far (opponents w/o rank)
                    .onErrorReturn { Right(opponents) }
}