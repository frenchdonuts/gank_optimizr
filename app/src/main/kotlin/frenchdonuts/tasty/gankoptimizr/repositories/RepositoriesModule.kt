package frenchdonuts.tasty.gankoptimizr.repositories

import dagger.Module
import dagger.Provides
import frenchdonuts.tasty.gankoptimizr.services.riot_service.RiotService
import javax.inject.Singleton

/**
 * Created by frenchdonuts on 5/9/2016.
 */
@Module
class RepositoriesModule {

    @Provides
    @Singleton
    fun providesRiotRepo(riotService: RiotService) = RiotRepo(riotService)
}