package frenchdonuts.tasty.gankoptimizr

import dagger.Component
import frenchdonuts.tasty.gankoptimizr.services.ServicesModule
import frenchdonuts.tasty.gankoptimizr.services.riot_service.RiotService
import frenchdonuts.tasty.gankoptimizr.core.AppState
import frenchdonuts.tasty.gankoptimizr.repositories.RepositoriesModule
import frenchdonuts.tasty.gankoptimizr.views.MainActivity
import frenchdonuts.tasty.gankoptimizr.views.OpponentsFragment
import frenchdonuts.tasty.gankoptimizr.views.SummonerNameFragment
import io.onedonut.re_droid.RDB
import javax.inject.Singleton

/**
 * Created by frenchdonuts on 4/25/16.
 */
@Singleton
@Component(modules = arrayOf(RDBModule::class, RepositoriesModule::class, ServicesModule::class))
interface ApplicationComponent {
    //
    fun inject(activity: MainActivity)

    fun inject(fragment: SummonerNameFragment)

    fun inject(fragment: OpponentsFragment)

    fun rdb(): RDB<AppState>
}