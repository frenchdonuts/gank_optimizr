package frenchdonuts.tasty.gankoptimizr

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import android.util.Log
import com.facebook.stetho.Stetho
import frenchdonuts.tasty.gankoptimizr.services.ServicesModule
import frenchdonuts.tasty.gankoptimizr.core.AppState
import io.onedonut.re_droid.utils.One

/**
 * Created by frenchdonuts on 4/24/16.
 */
class App : Application() {

    companion object {
        @JvmStatic lateinit var graph: ApplicationComponent
    }

    lateinit var appState: AppState
    override fun onCreate() {
        //
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        graph = DaggerApplicationComponent.builder()
                .rDBModule(RDBModule())
                .servicesModule(ServicesModule())
                .build()

        graph.rdb().execute { One(it) }.subscribe { Log.i("AppState", "${it._1}") }
    }

    override fun attachBaseContext(base: Context?) {
        //
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}